<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Search extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$search_key = $this->input->get('search_key');
		$exact = $this->input->get('exact');

		$this->load->view('header');
		$this->load->view('search_view', ['search_key' => $search_key, 'exact' => $exact]);
		$this->load->view('footer');

	}

	public function token_search() {

		$search_key = $this->input->get('search_key');

		$exact = $this->input->get('exact');

		$all = $this->input->post();

		$query = ' select ';

		$query .= ' tokens.token as "0",CONCAT("<a href=\'' . base_url() . 'search/corpus?token_id=",tokens.token_id,"\'>",tokens.token_count,"</a>") as "1" , ';

		$query .= ' "<a class=\'btn btn-primary meaning\'>শব্দার্থ</a>"  as "2"';

		$query .= ' from';

		$query .= ' tokens';

		$query .= ' WHERE';

		if ($exact == null) {

			$query .= '    (';

			/*
			*/

			$query .= '    tokens.token like "' . $search_key . '%"';

			$query .= '    OR tokens.token like "%' . $search_key . '%"';

			$query .= '    )';

			/*
			*/

			if ($all['order'][0]['column'] == 0) {

				$query .= ' ORDER BY case';

				$query .= ' WHEN tokens.token LIKE "' . $search_key . '%" THEN 1';

				$query .= ' WHEN tokens.token LIKE "%' . $search_key . '%" THEN 2';

				$query .= ' ELSE 3 END';

			}
		} else {
			$query .= '    tokens.token like "' . $search_key . '"';
		}

		$total = $this->db->query($query)->num_rows();

		if ($all['order'][0]['column'] == 1) {

			$query .= ' ORDER BY tokens.token_count ' . $all['order'][0]['dir'];

		}

		$query .= ' LIMIT ' . $all['start'] . ',' . $all['length'];

		echo json_encode(

			[

				"draw" => $all['draw'],

				"recordsTotal" => $total,

				"recordsFiltered" => $total,

				"data" => $this->db->query($query)->result_array(),

				"exact" => $this->input->get(),
			]

		);

	}

	public function corpus() {

		$token_id = $this->input->get('token_id');

		$this->db->select('*');

		$this->db->from('tokens');

		$this->db->where('token_id', $token_id);

		$this->load->view('header');
		$this->load->view('corpus_view', $this->db->get()->row_array());
		$this->load->view('footer');

	}

	public function corpus_search() {

		$token_id = $this->input->get('token_id');

		$all = $this->input->post();

		$this->db->select('files.article_name as "0"');

		$this->db->select('files.file_id as "1"');

		$this->db->select('pages.page_serial as "2"');

		$this->db->select('corpus.sentence_serial as "3"');

		$this->db->select('corpus.sentence_text as "4"');

		$this->db->from('corpus_tokens');

		$this->db->join('corpus', 'corpus.corpus_id = corpus_tokens.corpus_id', 'left');

		$this->db->join('pages', 'pages.page_id = corpus.page_id', 'left');

		$this->db->join('files', 'pages.file_id = files.file_id', 'left');

		$this->db->where('corpus_tokens.token_id', $token_id);

		$this->db->limit($all['length'], $all['start']);

		$this->db->order_by('`' . $all['order']['0']['column'] . '`', $all['order']['0']['dir']);

		$data = $this->db->get()->result_array();

		$this->db->select('corpus.*,files.*');

		$this->db->from('corpus_tokens');

		$this->db->join('corpus', 'corpus.corpus_id = corpus_tokens.corpus_id', 'left');

		$this->db->join('pages', 'pages.page_id = corpus.page_id', 'left');

		$this->db->join('files', 'pages.file_id = files.file_id', 'left');

		$this->db->where('corpus_tokens.token_id', $token_id);

		$total = $this->db->get()->num_rows();

		$token = $this->db->select('token')->from('tokens')->where('token_id', $token_id)->get()->row_array()['token'];

		foreach ($data as $key => $value) {
			$data[$key][4] = preg_replace('/\b' . $token . '(?!\w)/u', '<span style="background-color: #00ff00;">' . $token . '</span>', $value[4]);
		}

		echo json_encode(

			[

				"draw" => $all['draw'],

				"recordsTotal" => $total,

				"recordsFiltered" => $total,

				"data" => $data,

			]

		);

	}

	public function login() {

		$data = $this->input->post();

		if ($data['username'] == 'admin' && $data['password'] == '26-03-1971') {

			$this->session->set_userdata('logged_in', true);

			echo "1";

		} else {

			echo "0";

		}

	}

	public function logout() {
		$this->session->sess_destroy();
	}

}

/* End of file  */

/* Location: ./application/controllers/ */