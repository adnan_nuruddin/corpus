<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Articles extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{
		$this->load->view('header');
		$data = array("logged_in_user"=>$this->session->userdata('logged_in'));
		$this->load->view('article_list',$data);
		$this->load->view('footer');		
	}

	public function delete_article($id)
	{

		$this->db->trans_begin();

		// Delete corpus tokens first
		$this->db->select('corpus_token_id, token_id');
		$this->db->from('corpus_tokens');
		$this->db->join('corpus', 'corpus.corpus_id = corpus_tokens.corpus_id', 'left');
		$this->db->join('pages', 'pages.page_id = corpus.page_id', 'left');
		$this->db->where('pages.file_id', $id);
		$tokenData = $this->db->get()->result_array();

		foreach ($tokenData as $key => $value) {
			$this->db->delete('corpus_tokens', ["corpus_token_id"=>$value["corpus_token_id"]]);
			
			// decrease token count
		    $this->db->set('token_count', 'token_count - 1', FALSE);
		    $this->db->where('token_id',$value["token_id"]);
		    $this->db->update('tokens');
		}



		// Delete corpus
		$this->db->select('corpus_id');
		$this->db->from('corpus');
		$this->db->join('pages', 'pages.page_id = corpus.page_id', 'left');
		$this->db->where('pages.file_id', $id);
		$corpusData = $this->db->get()->result_array();

		foreach ($corpusData as $key => $value) {
			$this->db->delete('corpus', ["corpus_id"=>$value["corpus_id"]]);
		}

		// Delete page
		$this->db->select('page_id');
		$this->db->from('pages');
		$this->db->where('pages.file_id', $id);
		$corpusData = $this->db->get()->result_array();

		foreach ($corpusData as $key => $value) {
			$this->db->delete('pages', ["page_id"=>$value["page_id"]]);
		}


		// Delete file
		$this->db->delete('files', ["file_id"=>$id]);
		
		if ($this->db->trans_status() === FALSE)
		{
			echo json_encode(["success"=>false,"message"=>"Fil not deleted."]);
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(["success"=>true,"message"=>"Fil deleted. Token count: ".count($tokenData)]);
		}
	}


	public function article_search()
	{
		$all = $this->input->post();


		$this->db->select('files.file_id as "0"');
		$this->db->select('files.article_name as "1"');
		$this->db->select('files.article_source as "2"');
		$this->db->select('files.created_on as "3"');

		if($this->session->userdata('logged_in')){
			$this->db->select('CONCAT("<a id=\'delete_",files.file_id,"\' class=\'btn btn-danger btn-xs delete_article\'><i class=\'glyphicon glyphicon-trash\'></i></a>") as "5"');
		}

		$this->db->select('CONCAT("<a class=\'btn btn-primary btn-xs\' href=\''.base_url().'docs/",files.file_name,"\'><i class=\'glyphicon glyphicon-download-alt\'></i></a>") as "4"');
		$this->db->from('files');
		$this->db->limit($all['length'], $all['start']);
		$this->db->order_by('`'.$all['order'][0]['column'].'`', $all['order'][0]['dir']);
		if(trim($all['search']['value'])!=""){
			$this->db->where("(files.file_id LIKE '%".$all['search']['value']."%' OR files.article_name LIKE '%".$all['search']['value']."%' OR files.article_source LIKE '%".$all['search']['value']."%')");
		}
		$data = $this->db->get()->result_array();


		$this->db->select('files.article_name as "0"');
		$this->db->select('files.article_source as "1"');
		$this->db->select('files.created_on as "2"');

		$this->db->select('CONCAT("<a class=\'btn btn-primary btn-xs\' href=\''.base_url().'docs/",files.file_name,"\'><i class=\'glyphicon glyphicon-download-alt\'></i></a>") as "3"');
		$this->db->from('files');
		$total = $this->db->get()->num_rows();




		echo json_encode(
			[
				"draw"=>$all['draw'],
				"recordsTotal"=>$total,
				"recordsFiltered"=>$total,
				"data"=>$data,
				"all"=>$all
			]
		);
	}



}



/* End of file  */

/* Location: ./application/controllers/ */