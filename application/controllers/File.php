<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class File extends CI_Controller {
	public function index() {
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}
	public function upload_file() {
		$this->load->view('header');
		if ($this->session->userdata('logged_in')) {
			$this->load->view('file_view');
		} else {
			$this->load->view('login');
		}
		$this->load->view('footer');
	}
	public function file_upload_test() {
		$config['upload_path'] = './docs/';
		$config['allowed_types'] = 'docx';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		echo "<pre>";
		print_r($this->input->post());
		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
		} else {
			$data = array('upload_data' => $this->upload->data());
			$upload_data = array(
				'article_name' => $this->input->post('article_name'),
				'file_size' => $data['upload_data']['file_size'],
				'file_name' => $data['upload_data']['file_name'],
				'sentence_seperator' => ' ',
				'file_text' => ' ',
				'charecer_length' => 95,
				);
			$this->db->insert('files', $upload_data);
		}
	}
	public function file_upload() {
		define('K_TCPDF_EXTERNAL_CONFIG', true);
		define('K_PATH_IMAGES', dirname(__FILE__) . '/../images/');
		define('PDF_HEADER_LOGO', 'tcpdf_logo.jpg');
		define('PDF_HEADER_LOGO_WIDTH', 30);
		define('K_PATH_CACHE', sys_get_temp_dir() . '/');
		define('K_BLANK_IMAGE', '_blank.png');
		define('PDF_PAGE_FORMAT', 'A4');
		define('PDF_PAGE_ORIENTATION', 'P');
		define('PDF_CREATOR', 'TCPDF');
		define('PDF_AUTHOR', 'TCPDF');
		define('PDF_HEADER_TITLE', 'TCPDF Example');
		define('PDF_HEADER_STRING', "by Nicola Asuni - Tecnick.com\nwww.tcpdf.org");
		define('PDF_UNIT', 'mm');
		define('PDF_MARGIN_HEADER', 5);
		define('PDF_MARGIN_FOOTER', 10);
		define('PDF_MARGIN_TOP', 27);
		define('PDF_MARGIN_BOTTOM', 25);
		define('PDF_MARGIN_LEFT', 15);
		define('PDF_MARGIN_RIGHT', 15);
		define('PDF_FONT_NAME_MAIN', 'helvetica');
		define('PDF_FONT_SIZE_MAIN', 10);
		define('PDF_FONT_NAME_DATA', 'helvetica');
		define('PDF_FONT_SIZE_DATA', 8);
		define('PDF_FONT_MONOSPACED', 'courier');
		define('PDF_IMAGE_SCALE_RATIO', 1.25);
		define('HEAD_MAGNIFICATION', 1.1);
		define('K_CELL_HEIGHT_RATIO', 1.25);
		define('K_TITLE_MAGNIFICATION', 1.3);
		define('K_SMALL_RATIO', 2 / 3);
		define('K_THAI_TOPCHARS', true);
		define('K_TCPDF_CALLS_IN_HTML', true);
		define('K_TCPDF_THROW_EXCEPTION_ERROR', false);
		$this->load->library('tcpdf');
		$this->load->library('composer/autoload_real', 'pdf_read');
		//FILE UPLOAD START
		$config['upload_path'] = './docs/';
		$config['allowed_types'] = 'docx';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		$file_id = '';
		if (!$this->upload->do_upload('file')) {
		    echo "File Upload Error<br>";
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
			exit();
		} else {
		    echo "File Upload Success<br>";
			$file_data = array('upload_data' => $this->upload->data());
			$upload_data = array(
				'article_name' => $this->input->post('article_name'),
				'article_source' => $this->input->post('article_source'),
				'file_size' => $file_data['upload_data']['file_size'],
				'file_name' => $file_data['upload_data']['file_name'],
				'sentence_seperator' => ' ',
				'file_text' => ' ',
				'character_length' => 0,
				);
			$this->db->insert('files', $upload_data);
			$file_id = $this->db->insert_id();
		}
		//FILE UPLOAD END
		//READS DATA FROM DOCX FILE
		echo "File Read Start<br>";
		$filename = "docs/" . $file_data['upload_data']['file_name'];
		$content = $this->read_file_docx($filename);
		//DOCX READ END
		$this->db->update('files', ['file_text' => $content, 'character_length' => sizeof($content)], ['file_id' => $file_id]);
		//WRITE FILE TO PDF
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetFont('solaimanlipi', '', 12);
		$pdf->AddPage();
		$utf8text = $content;
		$pdf->SetTextColor(0, 63, 127);
		$pdf->Write(5, $utf8text, '', 0, '', false, 0, false, false, 0);
		$pdf->Output('pdf/example_008.pdf', 'F');
		//PDF WRIING END
		//READ FILE FROM PDF
		$parser = new \Smalot\PdfParser\Parser();
		$pdf = $parser->parseFile(base_url() . 'pdf/example_008.pdf');
		$pages = $pdf->getPages();
		//PDF READING END
		$query = "SET collation_connection = utf8_general_ci;";
		foreach ($pages as $key => $page) {
			$this->db->insert('pages', ['file_id' => $file_id, 'page_serial' => ($key + 1)]);
			$page_id = $this->db->insert_id();
			$lines = preg_split("/\\r\\n|\\r|\\n/", $page->getText());
			foreach ($lines as $k => $value) {
			    if(trim($value) == '' || $value == 'Powered by TCPDF (www.tcpdf.org)'){
			        continue;
			    }
				$this->db->insert('corpus', array('sentence_serial' => ($k + 1), 'sentence_text' => $value, 'word_seperator' => ' ', 'page_id' => $page_id));
				$line_id = $this->db->insert_id();
				$temp_num = trim($this->replace_numbers_and_special_characters($value));
				$tokens = explode(' ', $temp_num);
				$custom_id = uniqid();
				foreach ($tokens as $k1 => $v1) {
					if (trim($v1 != '')) {
						$query .= "INSERT INTO tokens (token) VALUES ('$v1') ON DUPLICATE KEY UPDATE token_count=token_count+1;
						";
						$query .= "INSERT INTO corpus_tokens  (corpus_id,token_id) values ('$line_id',(select token_id from tokens where token='$v1'));
						";
					}
				}
			}
		}
        
        $link = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        mysqli_set_charset ( $link , 'utf8' );
		// var_dump($query);
        mysqli_multi_query($link, $query);
	}
	public function read_file_docx($filename) {
		$striped_content = '';
		$content = '';
		if (!$filename || !file_exists($filename)) {
			return false;
		}
		$zip = zip_open($filename);
		if (!$zip || is_numeric($zip)) {
			return false;
		}
		while ($zip_entry = zip_read($zip)) {
			if (zip_entry_open($zip, $zip_entry) == FALSE) {
				continue;
			}
			if (zip_entry_name($zip_entry) != "word/document.xml") {
				continue;
			}
			$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			zip_entry_close($zip_entry);
		}
		zip_close($zip);
		$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
		$content = str_replace('</w:r></w:p>', "\r\n", $content);
		$striped_content = strip_tags($content);
		return $striped_content;
	}
	public function replace_numbers_and_special_characters($text) {
		// $text = str_replace('০', '', $text);
		// $text = str_replace('১', '', $text);
		// $text = str_replace('২', '', $text);
		// $text = str_replace('৩', '', $text);
		// $text = str_replace('৪', '', $text);
		// $text = str_replace('৫', '', $text);
		// $text = str_replace('৬', '', $text);
		// $text = str_replace('৭', '', $text);
		// $text = str_replace('৮', '', $text);
		// $text = str_replace('৯', '', $text);
		$text = str_replace('-', '', $text);
		$text = str_replace('=', '', $text);
		$text = str_replace('+', '', $text);
		$text = str_replace('-', '', $text);
		$text = str_replace('।', '', $text);
		$text = str_replace('\\', '', $text);
		$text = str_replace('/', '', $text);
		$text = str_replace(',', '', $text);
		$text = str_replace('!', '', $text);
		$text = str_replace('?', '', $text);
		$text = str_replace(':', '', $text);
		$text = str_replace(';', '', $text);
		$text = str_replace('(', '', $text);
		/*
		*/
		$text = str_replace(')', '', $text);
		$text = str_replace('{', '', $text);
		$text = str_replace('}', '', $text);
		$text = str_replace('[', '', $text);
		$text = str_replace(']', '', $text);
		$text = str_replace('<', '', $text);
		$text = str_replace('>', '', $text);
		$text = str_replace('’', '', $text);
		$text = str_replace('‘', '', $text);
		return $text;
	}
}
