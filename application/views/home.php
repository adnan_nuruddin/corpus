<nav class="navbar navbar-inverse navbar-fixed-top">

	<div class="container">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

				<span class="sr-only">Toggle navigation</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

			</button>

			<a class="navbar-brand" href="<?php echo base_url() ?>">Bangla Written Corpus 2.0</a>

		</div>

		<div id="navbar" class="navbar-collapse collapse">

			<ul class="nav navbar-nav">

				<li class="active"><a href="<?php echo base_url() ?>">Home</a></li>

				<li><a href="<?php echo base_url() . 'file/upload_file' ?>">Upload File</a></li>

				<li><a href="<?php echo base_url() . 'articles' ?>">Text List</a></li>

				<li><a href="<?php echo base_url() . 'search' ?>">Token List</a></li>

			</ul>

			<form method="get" action="<?php echo site_url('/search') ?>" class="navbar-form navbar-right">

				<div class="form-group">

					<input type="text" name="search_key" placeholder="Search token" class="form-control">

				</div>

								<button type="submit" class="btn btn-info">Search</button>
				<div class="checkbox">
					<label style="color:white">
						<input type="checkbox" name="exact">
						Exact Match
					</label>
				</div>

			</form>

		</div><!--/.navbar-collapse -->

	</div>

</nav>
<!-- Main jumbotron for a primary marketing message or call to action -->

<div style="height:86vh" class="jumbotron">

	<div class="container">

		<p class=MsoNormal><span style='font-family:"Times New Roman",serif'>The Bangla

			Written Corpus is composed of 20156 words in 33 texts. These texts were

			published in 2015 and 2016. The samples of this written corpus were collected

			from different sources, such as, newspapers, academic journal and literary

			journal.<o:p></o:p></span>

		</p>



		<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span

			style='font-family:"Times New Roman",serif'>Newspaper</span></u></b><span

			style='font-family:"Times New Roman",serif'>: News articles were collected from

			three different newspapers named ‘the Daily <span class=SpellE>Prothom</span> <span

			class=SpellE>Alo</span>’, ‘the Daily <span class=SpellE>Samakal</span>’, and ‘the

			Daily Bangladesh <span class=SpellE>Pratidin</span>’. These newspapers were

			selected because of their popularity and acceptance among the Bangla newspaper

			readers. Lead news, editorial and feature news were the types of collected

			samples. All news articles were published in the year 2016.<o:p></o:p></span>

		</p>



		<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span

			style='font-family:"Times New Roman",serif'>Academic Journal</span></u></b><span

			style='font-family:"Times New Roman",serif'>: An academic journal-‘The Dhaka

			University Journal of Linguistics’ was selected for the collection of academic

			samples. Collected three articles were written on language and linguistics.

			These articles were published in the year 2016.<o:p></o:p></span>

		</p>



		<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span

			style='font-family:"Times New Roman",serif'>Literary Journal</span></u></b><span

			style='font-family:"Times New Roman",serif'>: An article and two stories were

			collected from the literary journal ‘<span class=SpellE>Ontordesh</span>’. This

			journal was published in the year 2015.<o:p></o:p></span>

		</p>



		<p class=MsoNormal><span style='font-family:"Times New Roman",serif'>The total

			number of words in the news samples is 10704. The total number of words in the

			articles and literature (stories) are 6984 and 2468 respectively.<o:p></o:p></span>

		</p>



		<p class=MsoNormal><span style='font-family:"Times New Roman",serif'>One can go

			to the search option and search for a single word and one will be able to see

			the frequency and context of the word. The sample texts are also available in

			the text option. <o:p></o:p></span>

		</p>

	</div>

</div>


<!-- Bootstrap core JavaScript  ================================================== -->

<!-- Placed at the end of the document so the pages load faster -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

<script src="http://malsup.github.com/jquery.form.js"></script>

<script>

        // wait for the DOM to be loaded

        $(document).ready(function() {

            // bind 'myForm' and provide a simple callback function

            $('#upload_form').ajaxForm({

            	error: function() {

            		alert("Something went wrong");

            	},

            	beforeSubmit: function(arr, $form, options) {

            		alert('Wait while the form is being submitted');

            	},

            	success: function() {

            		alert("File upload successful");

            		$('#upload_form')[0].reset();

            	}

            });

        });

        </script>

