<nav class="navbar navbar-inverse navbar-fixed-top">

	<div class="container">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

				<span class="sr-only">Toggle navigation</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

			</button>

			<a class="navbar-brand" href="<?php echo base_url() ?>">Bangla Written Corpus</a>

		</div>

		<div id="navbar" class="navbar-collapse collapse">

			<ul class="nav navbar-nav">

				<li><a href="<?php echo base_url() ?>">Home</a></li>

				<li><a href="<?php echo base_url() . 'file/upload_file' ?>">Upload File</a></li>

				<li><a href="<?php echo base_url() . 'articles' ?>">Text List</a></li>

				<li><a href="<?php echo base_url() . 'search' ?>">Token List</a></li>

			</ul>

			<form method="get" action="<?php echo site_url('/search') ?>" class="navbar-form navbar-right">

				<div class="form-group">

					<input type="text" name="search_key" placeholder="Search token" class="form-control">

				</div>

								<button type="submit" class="btn btn-info">Search</button>
				<div class="checkbox">
					<label style="color:white">
						<input type="checkbox" name="exact">
						Exact Match
					</label>
				</div>

			</form>

		</div><!--/.navbar-collapse -->

	</div>

</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->

<div style="height:86vh" class="jumbotron">

	<div class="container">

		<h3>Corpus found for "<?php echo $token ?>"</h3>

		<table id="example" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">

			<thead>

				<tr>

					<th>Text Name</th>

					<th>Text Serial</th>

					<th>Page Serial</th>

					<th>Sentence Serial</th>

					<th>Sentence</th>

				</tr>

			</thead>

			<tbody>

			</tbody>

		</table>

	</div>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script>

	$(document).ready(function() {

		$('#example').dataTable({

			"bPaginate": true,

			"bLengthChange": false,

			"bFilter": false,

			"bInfo": true,

			"bAutoWidth": false,

			'serverSide': true,

			'draw' : highlighter('<?php echo $token ?>'),

			'ajax': {

				'url': '<?php echo site_url("/search/corpus_search") . "?token_id=" . $token_id ?>',

				'type': 'POST'

			}

		});



		// $('#example').on( 'draw.dt', function () {

		// 	highlighter('<?php echo $token ?>');

		// });





		function highlighter(key){

			$('table:first tr').each(function(){

				$(this).find('td:last').each(function(i,e) {

					var text = "";

					var temp  = $(e).text().split(' ');



					$.each(temp,function(index, el) {

						if(el==key)

						{

							el = '<span style="background-color:yellow">'+key+'</span>';

						}

						text+=(el+" ");

					});



					$(e).html(text);

				});

			});

		};

	});

</script>