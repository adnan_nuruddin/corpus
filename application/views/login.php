<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url() ?>">Bangla Written Corpus</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url() ?>">Home</a></li>
				<li class="active"><a href="<?php echo base_url() . 'file/upload_file' ?>">Upload File</a></li>
				<li><a href="<?php echo base_url() . 'articles' ?>">Text List</a></li>
				<li><a href="<?php echo base_url() . 'search' ?>">Token List</a></li>
			</ul>
			<form method="get" action="<?php echo site_url('/search') ?>" class="navbar-form navbar-right">
				<div class="form-group">
					<input type="text" name="search_key" placeholder="Search token" class="form-control">
				</div>
								<button type="submit" class="btn btn-info">Search</button>
				<div class="checkbox">
					<label style="color:white">
						<input type="checkbox" name="exact">
						Exact Match
					</label>
				</div>
			</form>
		</div><!--/.navbar-collapse -->
	</div>
</nav>
<div class="container">
	<div class="row">
		<div class='col-md-3'></div>
		<div class="col-md-6">
			<div class="login-box well">
				<form id="login_form" method="post" action="<?php echo base_url() ?>search/login">
					<legend>Sign In</legend>
					<div class="form-group">
						<label for="username">Username</label>
						<input name="username" value='' id="username" placeholder="E-mail or Username" type="text" class="form-control" />
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input name="password" id="password" value='' placeholder="Password" type="password" class="form-control" />
					</div>
<!-- 						<div class="input-group">
							<div class="checkbox">
								<label>
									<input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
								</label>
							</div>
						</div> -->
						<div class="form-group">
							<input type="submit" class="btn btn-default btn-login-submit btn-block m-t-md" value="Login" />
						</div>
<!-- 						<span class='text-center'><a href="/resetting/request" class="text-sm">Forgot Password?</a></span>
						<div class="form-group">
							<p class="text-center m-t-xs text-sm">Do not have an account?</p>
							<a href="/register/" class="btn btn-default btn-block m-t-md">Create an account</a>
						</div> -->
					</form>

				</div>
			</div>
			<div class='col-md-3'></div>
		</div>
	</div>

</div>

<!-- Bootstrap core JavaScript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script>
	$(document).ready(function() {

		$('#login_form').ajaxForm({
			error: function() {
				alert("Something went wrong. Please try again");
			},
			beforeSubmit: function(arr, $form, options) {

			},
			success: function(data) {
				if (data==1) {
					alert("login successful");
					location.reload();
				} else{
					alert("invalid username or password")
				};
			}
		});
	});
</script>
