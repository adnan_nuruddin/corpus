<nav class="navbar navbar-inverse navbar-fixed-top">

	<div class="container">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

				<span class="sr-only">Toggle navigation</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

			</button>

			<a class="navbar-brand" href="<?php echo base_url() ?>">Bangla Written Corpus</a>

		</div>

		<div id="navbar" class="navbar-collapse collapse">

			<ul class="nav navbar-nav">

				<li><a href="<?php echo base_url() ?>">Home</a></li>

				<li class="active"><a href="<?php echo base_url() . 'file/upload_file' ?>">Upload File</a></li>

				<li><a href="<?php echo base_url() . 'articles' ?>">Text List</a></li>

				<li><a href="<?php echo base_url() . 'search' ?>">Token List</a></li>

			</ul>

			<form method="get" action="<?php echo site_url('/search') ?>" class="navbar-form navbar-right">

				<div class="form-group">

					<input type="text" name="search_key" placeholder="Search token" class="form-control">

				</div>

								<button type="submit" class="btn btn-info">Search</button>
				<div class="checkbox">
					<label style="color:white">
						<input type="checkbox" name="exact">
						Exact Match
					</label>
				</div>

			</form>

		</div><!--/.navbar-collapse -->

	</div>

</nav>
<!-- Main jumbotron for a primary marketing message or call to action -->

<div style="height:86vh" class="jumbotron">


	<form class="form-horizontal" id="upload_form" enctype="multipart/form-data" method="post" action="<?php echo site_url('/file/file_upload') ?>">

		<div class="form-group">

			<label for="inputEmail3" class="col-sm-2 control-label">Text name</label>

			<div class="col-sm-8">

				<input type="text" class="form-control" name="article_name" placeholder="Article">

			</div>

		</div>

		<div class="form-group">

			<label for="inputEmail3" class="col-sm-2 control-label">Text Source</label>

			<div class="col-sm-8">

				<textarea class="form-control" name="article_source" placeholder="Article Source"></textarea>

			</div>

		</div>



		<div class="form-group">

			<label for="inputPassword3" class="col-sm-2 control-label">Text file</label>

			<div class="col-sm-8">

				<input type="file" class="form-control" name="file">

			</div>

		</div>

		<div class="form-group">

			<div class="col-sm-offset-2 col-sm-8">

				<div class="checkbox">

					<label>

						<input type="checkbox" required> I own all copyrights of this file.

					</label>

				</div>

			</div>

		</div>

		<div class="form-group">

			<div class="col-sm-offset-2 col-sm-8">

				<button type="submit" class="btn btn-success">Submit</button>
				<button id="logout" class="btn btn-danger">logout</button>

			</div>

		</div>

	</form>


</div>

</div>



<!-- Bootstrap core JavaScript   ================================================== -->

<!-- Placed at the end of the document so the pages load faster -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

<script>

    // wait for the DOM to be loaded

    $(document).ready(function() {

        // bind 'myForm' and provide a simple callback function

        $('#upload_form').ajaxForm({

        	error: function() {

        		alert("Something went wrong");

        	},

        	beforeSubmit: function(arr, $form, options) {

        		alert('Wait while the form is being submitted');

        	},

        	success: function() {

        		alert("File upload successful");

        		$('#upload_form')[0].reset();

        	}

        });

        $('#logout').click(function(event) {
        	event.preventDefault();
        	$.get('<?php echo base_url() ?>search/logout', function(data) {
        		location.reload();
        	});
        });

    });

</script>