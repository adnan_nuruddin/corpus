<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<meta name="description" content="The Bangla Written Corpus is composed of 20156 words in 33 texts. These texts were published in 2015 and 2016. The samples of this written corpus were collected from different sources, such as, newspapers, academic journal and literary journal.">

	<meta name="author" content="">

	<meta name="keywords" content="bangla corpus, corpus, bangla, language">

	<link rel="icon" href="../../favicon.jpg">



	<title>Bangla Written Corpus</title>



	<!-- Bootstrap core CSS -->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


	<!-- Custom styles for this template -->



	<style type="text/css" media="screen">

	@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700);

	body {
  	
  	padding-top: 50px;
  	
  	padding-bottom: 20px;
	
	}

	.well{

		background-color:#fff!important;

		border-radius:0!important;

		border:black solid 1px;

	}



	.well.login-box {

		/*width:400px; */

		border:#d1d1d1 solid 1px;

		margin:0 auto;

		margin-top:30px;

	}

	.well.login-box legend {

		font-size:26px;

		text-align:center;

		font-weight:300;

	}

	.well.login-box label {

		font-weight:300;

		font-size:13px;



	}

	.well.login-box input[type="text"] {

		box-shadow:none;

		border-color:#ddd;

		border-radius:0;

	}



	.well.welcome-text{

		font-size:21px;

	}



	/* Notifications */



	.notification{

		position:fixed;

		top: 20px;

		right:0;

		background-color:#FF4136;

		padding: 20px;

		color: #fff;

		font-size:21px;

		display:none;

	}

	.notification-success{

		background-color:#3D9970;

	}



	.notification-show{

		display:block!important;

	}



	/*Loged in*/

	.btn-default {

		color: #333;

		background-color: #f9f9f9;

		border-color: #ccc;

		border: 1px solid;

		text-align: center;

		cursor: pointer;

		color: #5e5e5e;

		-moz-border-radius: 3px;

		-webkit-border-radius: 3px;

		border-radius: 3px;

		background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #fefefe), color-stop(100%, #f9f9f9)), #f9f9f9;

		background: -moz-linear-gradient(#fefefe, #f9f9f9), #f9f9f9;

		background: -webkit-linear-gradient(#fefefe, #f9f9f9), #f9f9f9;

		background: linear-gradient(#fefefe, #f9f9f9), #f9f9f9;

		border-color: #c3c3c3 #c3c3c3 #bebebe;

		-moz-box-shadow: rgba(0, 0, 0, 0.06) 0 1px 0, rgba(255, 255, 255, 0.1) 0 1px 0 0 inset;

		-webkit-box-shadow: rgba(0, 0, 0, 0.06) 0 1px 0, rgba(255, 255, 255, 0.1) 0 1px 0 0 inset;

		box-shadow: rgba(0, 0, 0, 0.06) 0 1px 0, rgba(255, 255, 255, 0.1) 0 1px 0 0 inset;

	}

	.jumbotron{
		background: none;
	}

	.table{
		background-color: #eee;
	}

	body{
		background: url(http://www.banglacorpus.com/paisley.png);
	}

	#example_filter{
		display: none !important;
	}

	body {
		padding-top: 50px;
		padding-bottom: 20px;
	}

</style>


</head>

<body>
