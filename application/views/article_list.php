<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url() ?>">Bangla Written Corpus</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url() ?>">Home</a></li>
				<li><a href="<?php echo base_url() . 'file/upload_file' ?>">Upload File</a></li>
				<li class="active"><a href="<?php echo base_url() . 'articles' ?>">Text List</a></li>
				<li><a href="<?php echo base_url() . 'search' ?>">Token List</a></li>
			</ul>
			<form method="get" action="<?php echo site_url('/search') ?>" class="navbar-form navbar-right">
				<div class="form-group">
					<input type="text" name="search_key" placeholder="Search token" class="form-control">
				</div>
				<button type="submit" class="btn btn-info">Search</button>
				<div class="checkbox">
					<label style="color:white">
						<input type="checkbox" name="exact">
						Exact Match
					</label>
				</div>
			</form>
		</div><!--/.navbar-collapse -->
	</div>
</nav>
<!-- Main jumbotron for a primary marketing message or call to action -->
<div style="height:86vh" class="jumbotron">
	<div class="container">
		<table id="articleList" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
			<thead>
				<tr>
					<td colspan="5">
						<input type="text" style='width:100%' placeholder="Search text" id="article_search" class="form-control">
					</td>
				</tr>
				<tr>
					<th>ID #</th>
					<th>Text Name</th>
					<th>Text Source</th>
					<th>Added on</th>
					<th>Download</th>
					<?php if ($logged_in_user): ?>
						<th>Delete</th>					
					<?php endif ?>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			var oTable = $('#articleList').DataTable({
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": true,
				"bAutoWidth": false,
				'serverSide': true,
				'ajax': {
					'url': '<?php echo site_url("/articles/article_search") ?>',
					'type': 'POST'
				}
			});
			$('#article_search').keyup(function(){
				oTable.search($(this).val()).draw() ;
			})
		});
	</script>

	<script type="text/javascript">
		function isJson(str) {
			try {
				JSON.parse(str);
			} catch (e) {
				return false;
			}
			return true;
		}

		$( "#articleList" ).on( "click", ".delete_article", function() {
			if (confirm('Are you sure you want to delete this file?')) {
				alert("File being deleted. Please wait.");
				$.get("/articles/delete_article/"+$( this ).attr('id').split('_')[1],function(data){
					if(isJson(data)){
						data = JSON.parse(data);
						if(data.success){
							alert(data.message);
							setTimeout(()=>{location.reload()}, 1000);
						} else{
							alert(data.message);
						}	  					
					} else {
						alert("Server error. "+ data);
					}
				});
			}
		});	
	</script>
</body>
</html>
